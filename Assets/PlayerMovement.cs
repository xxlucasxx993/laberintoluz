using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 5f;
    private Rigidbody2D rb;
    private Vector2 movement;

    Animator animPlayer ;
    string currentAnimation = "waos" ;
    bool canMove = true ;

    SpriteRenderer spRenPlayer ;

    IEnumerator Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animPlayer = GetComponent < Animator > () ;
        spRenPlayer = GetComponent < SpriteRenderer > () ;

        yield return null ;
        CambiarAnimacion ( "Idle" ) ;
    }

    void Update()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");

        if ( Input.GetAxisRaw("Horizontal") > 0f )
        spRenPlayer.flipX = false ;

        if ( Input.GetAxisRaw("Horizontal") < 0f )
        spRenPlayer.flipX = true ;


        ManageAnimations () ;

        if ( Input.GetKeyDown ( KeyCode.F ) && canMove )
        StartCoroutine ( DeathSecuence () ) ;
    }

    void FixedUpdate()
    {
        //movimiento :v
        if ( canMove )
        rb.MovePosition(rb.position + movement * moveSpeed * Time.fixedDeltaTime);
    }

    void ManageAnimations ()
    {
        if ( canMove )  {
            if ( Input.GetAxisRaw("Horizontal") == 0f && Input.GetAxisRaw("Horizontal") == 0f )
            CambiarAnimacion ( "Idle" ) ;
            else
            CambiarAnimacion ( "Run" ) ;
        }
    }

    void CambiarAnimacion ( string newState )
    {
        if ( newState != currentAnimation )
        animPlayer.Play ( newState ) ;
    }

    IEnumerator DeathSecuence ()
    {
        canMove = false ;
        yield return null ;

        CambiarAnimacion ( "Death" ) ;
        yield return new WaitForSeconds ( 5f ) ;

        SceneManager.LoadScene ( "EP3Level" ) ;
    }
}