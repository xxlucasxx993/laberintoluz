using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public class PlayerLightInteraction : MonoBehaviour
{
    public float intensityChangeSpeed = 0.5f;
    public string cuarto1ObjectName = "Cuarto1"; 
    public string cuarto2ObjectName = "Cuarto2"; 
    public string cuarto3ObjectName = "Cuarto3";
    public string cuarto4ObjectName = "Cuarto4"; 
    public string doorObjectName = "Door"; 
    public string hDoorObjectName = "HDoor"; 
    public string fDoorObjectName = "FDoor"; 

    private GameObject cuarto1Object;
    private GameObject cuarto2Object; 
    private GameObject cuarto3Object; 
    private GameObject cuarto4Object;
    private GameObject doorObject; 
    private GameObject hDoorObject; 
    private GameObject fDoorObject; 

    private UnityEngine.Rendering.Universal.Light2D cuarto1Light; 
    private UnityEngine.Rendering.Universal.Light2D cuarto2Light; 
    private UnityEngine.Rendering.Universal.Light2D cuarto3Light;
    private UnityEngine.Rendering.Universal.Light2D cuarto4Light;

    private bool lightItemCollected = false; 
    private bool lightItem1Collected = false; 
    private bool lightItem2Collected = false; 
    private bool lightItem3Collected = false; 

    private void Start()
    {
        cuarto1Object = GameObject.Find(cuarto1ObjectName);
        if (cuarto1Object != null)
        {
            cuarto1Light = cuarto1Object.GetComponent<UnityEngine.Rendering.Universal.Light2D>();
            if (cuarto1Light != null)
            {
                cuarto1Light.intensity = 0f;
            }
            else
            {
                Debug.LogError("No se encontró el componente Light2D en el objeto Cuarto1.");
            }
        }
        else
        {
            Debug.LogError("No se encontró el objeto Cuarto1 con el nombre: " + cuarto1ObjectName);
        }

        cuarto2Object = GameObject.Find(cuarto2ObjectName);
        if (cuarto2Object != null)
        {
            cuarto2Light = cuarto2Object.GetComponent<UnityEngine.Rendering.Universal.Light2D>();
            if (cuarto2Light != null)
            {
                cuarto2Light.intensity = 0f;
            }
            else
            {
                Debug.LogError("No se encontró el componente Light2D en el objeto Cuarto2.");
            }
        }
        else
        {
            Debug.LogError("No se encontró el objeto Cuarto2 con el nombre: " + cuarto2ObjectName);
        }

        cuarto3Object = GameObject.Find(cuarto3ObjectName);
        if (cuarto3Object != null)
        {
            cuarto3Light = cuarto3Object.GetComponent<UnityEngine.Rendering.Universal.Light2D>();
            if (cuarto3Light != null)
            {
                cuarto3Light.intensity = 0f;
            }
            else
            {
                Debug.LogError("No se encontró el componente Light2D en el objeto Cuarto3.");
            }
        }
        else
        {
            Debug.LogError("No se encontró el objeto Cuarto3 con el nombre: " + cuarto3ObjectName);
        }

        cuarto4Object = GameObject.Find(cuarto4ObjectName);
        if (cuarto4Object != null)
        {
            cuarto4Light = cuarto4Object.GetComponent<UnityEngine.Rendering.Universal.Light2D>();
            if (cuarto4Light != null)
            {
                cuarto4Light.intensity = 0f;
            }
            else
            {
                Debug.LogError("No se encontró el componente Light2D en el objeto Cuarto4.");
            }
        }
        else
        {
            Debug.LogError("No se encontró el objeto Cuarto4 con el nombre: " + cuarto4ObjectName);
        }

        doorObject = GameObject.Find(doorObjectName);
        if (doorObject == null)
        {
            Debug.LogError("No se encontró el objeto Puerta con el nombre: " + doorObjectName);
        }

        hDoorObject = GameObject.Find(hDoorObjectName);
        if (hDoorObject == null)
        {
            Debug.LogError("No se encontró el objeto HDoor con el nombre: " + hDoorObjectName);
        }

        fDoorObject = GameObject.Find(fDoorObjectName);
        if (fDoorObject == null)
        {
            Debug.LogError("No se encontró el objeto FDoor con el nombre: " + fDoorObjectName);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Luces") && !lightItemCollected)
        {
            Destroy(collision.gameObject);

            if (cuarto1Object != null && !cuarto1Object.activeSelf)
            {
                cuarto1Object.SetActive(true);
            }

            if (doorObject != null)
            {
                Destroy(doorObject);
            }
            else
            {
                Debug.LogError("No se encontró el objeto Puerta con el nombre: " + doorObjectName);
            }

            StartCoroutine(ChangeIntensityOverTime(cuarto1Light));

            lightItemCollected = true;
        }
        else if (collision.CompareTag("Luces1") && !lightItem1Collected)
        {
            Destroy(collision.gameObject);

            if (hDoorObject != null)
            {
                Destroy(hDoorObject);
            }
            else
            {
                Debug.LogError("No se encontró el objeto HDoor con el nombre: " + hDoorObjectName);
            }

            if (cuarto2Object != null && !cuarto2Object.activeSelf)
            {
                cuarto2Object.SetActive(true);
            }

            StartCoroutine(ChangeIntensityOverTime(cuarto2Light));

            lightItem1Collected = true;
        }
        else if (collision.CompareTag("Luces2") && !lightItem2Collected)
        {
            Destroy(collision.gameObject);

            if (fDoorObject != null)
            {
                Destroy(fDoorObject);
            }
            else
            {
                Debug.LogError("No se encontró el objeto FDoor con el nombre: " + fDoorObjectName);
            }

            if (cuarto3Object != null && !cuarto3Object.activeSelf)
            {
                cuarto3Object.SetActive(true);
            }

            StartCoroutine(ChangeIntensityOverTime(cuarto3Light));

            lightItem2Collected = true;
        }
        else if (collision.CompareTag("Luces3") && !lightItem3Collected)
        {
            Destroy(collision.gameObject);

            if (cuarto4Object != null && !cuarto4Object.activeSelf)
            {
                cuarto4Object.SetActive(true);
            }

            StartCoroutine(ChangeIntensityOverTime(cuarto4Light));

            lightItem3Collected = true;
        }
    }

    private IEnumerator ChangeIntensityOverTime(UnityEngine.Rendering.Universal.Light2D targetLight)
    {
        float startIntensity = targetLight.intensity;
        
        float targetIntensity = 1f;

        float elapsedTime = 0f;

        while (elapsedTime < 1f)
        {
            elapsedTime += Time.deltaTime * intensityChangeSpeed;

            targetLight.intensity = Mathf.Lerp(startIntensity, targetIntensity, elapsedTime);

            yield return null;
        }
        targetLight.intensity = targetIntensity;
    }
}
