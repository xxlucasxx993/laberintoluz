using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowDelay : MonoBehaviour
{
    public Transform target; // El objeto que la cámara seguirá (el jugador)
    public Vector3 offset;   // La posición de desplazamiento de la cámara con respecto al jugador
    public float smoothSpeed = 0.125f; // Velocidad de suavizado (puede ajustarse en el Inspector)

    void LateUpdate()
    {
        if (target != null)
        {
            Vector3 desiredPosition = target.position + offset;
            Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed);
            transform.position = smoothedPosition;
        }
    }
}
